# Release Notes for JavaFX 15.0.1

## Introduction

The following notes describe important changes and information about this release. In some cases, the descriptions provide links to additional detailed information about an issue or a change.

As of JDK 11 the JavaFX modules are delivered separately from the JDK. These release notes cover the standalone JavaFX 15.0.1 release. As such, they complement the [JavaFX 15 Release Notes](https://github.com/openjdk/jfx/blob/jfx15/doc-files/release-notes-15.md).

JavaFX 15.0.1 requires JDK 11 or later.

## List of Fixed Bugs
Issue key|Summary|Subcomponent
---------|-------|------------
| [JDK-8252381](https://bugs.openjdk.java.net/browse/JDK-8252381) | Cherry pick GTK WebKit 2.28.4 changes | web          |
| [JDK-8249839](https://bugs.openjdk.java.net/browse/JDK-8249839) | Cherry pick GTK WebKit 2.28.3 changes | web          |
| [JDK-8245284](https://bugs.openjdk.java.net/browse/JDK-8245284) | Update to 610.1 version of WebKit     | web          |


## List of Security fixes
Issue key|Summary|Subcomponent
---------|-------|------------
[JDK-8248177](not public) | Improve XML support | web          
